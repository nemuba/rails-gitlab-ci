class ClocksController < ApplicationController
  include ClocksHelper

  def index
    @clocks = current_user.clocks
  end

  def new
    @clock = current_user.clocks.new
  end

  def create
    @clock = current_user.clocks.new(clock_params)
    if @clock.save
      redirect_to clocks_path
    else
      render :index, notice: "Clock not created"
    end
  end

  private

  def clock_params
    params.require(:clock).permit(:description, :duration_time)
  end
end
