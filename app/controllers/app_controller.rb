# frozen_string_literal: true

# AppController
class AppController < ApplicationController
  def index; end
end
