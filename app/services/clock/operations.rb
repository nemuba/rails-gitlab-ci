# frozen_string_literal: true

module Clock::Operations
  def create_clock
    clock = current_user.clocks.new(params)
    clock.save
  end

  def update_clock
    clock = current_user.clocks.find(params[:id])
    clock.update(params) if clock.present?
  end

  def destroy_clock
    clock = current_user.clocks.find(params[:id])
    clock.destroy if clock.present?
  end
end
