# frozen_string_literal: true

module Clock::Format
  def format_to_seconds(time)
    raise_time_error unless time_valid?(time)

    time = time.split(" ")
    seconds = 0
    time.each do |t|
      if t.include?("h")
        hours = t.to_i
        seconds += hours * 3600
      elsif t.include?("m")
        minutes = t.to_i
        seconds += minutes * 60
      elsif t.include?("s")
        seconds += t.to_i
      end
    end
    seconds
  end

  def seconds_to_format(seconds)
    hours = seconds / 3600
    seconds %= 3600
    minutes = seconds / 60
    seconds %= 60

    time = ""
    time += "#{hours}h " if hours.positive?
    time += "#{minutes}m " if minutes.positive?
    time += "#{seconds}s" if seconds.positive?
    time.strip
  end

  private

  def raise_time_error
    raise ArgumentError, "Invalid time format. Please use the format '12h 30m 30s'"
  end

  def time_valid?(time)
    case time.split(" ").size
    when 3
      time.match?(/\d{1,2}[h] \d{1,2}[m] \d{1,2}[s]/)
    when 2
      time.match?(/\d{1,2}[h] \d{1,2}[m]/) || time.match?(/\d{1,2}[m] \d{1,2}[s]/)
    when 1
      time.match?(/\d{1,2}[hms]/)
    else
      false
    end
  end
end
