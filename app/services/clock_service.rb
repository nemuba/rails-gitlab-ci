# frozen_string_literal: true

# ClockService
# This service is responsible for handling all the clock related operations
# like translating the string time to integer time and vice versa
# Example:
# 1. 12m 30s => 750
# 2. 12m => 720
# 3. 1h 12m 30s => 4350
# 4. 1h => 3600
class ClockService
  include Clock::Operations
  extend Clock::Format

  attr_accessor :params, :current_user

  def initialize(params, current_user)
    @params = params
    @current_user = current_user
  end
end
