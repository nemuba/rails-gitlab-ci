import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="clock"
export default class extends Controller {
  connect() {
    console.log('Hello, Stimulus!')
  }
}
