class Clock < ApplicationRecord
  include Format
  # Relationships ...............................
  belongs_to :user

  # Enums .......................................
  enum_for :status, %w[running paused stopped]

  # Attributes ..................................
  attribute :duration_time, :string

  # Validations .................................
  validates :description, presence: true
  validates :start_time, presence: true
  validates :status, presence: true, inclusion: { in:  %w[running paused stopped] }

  # Callbacks ..................................
  after_initialize :set_start_time
  after_initialize :set_duration
  after_create :set_end_time
  before_update :set_end_time

  private

  def set_start_time
    self.start_time ||= Time.current
  end

  def set_end_time
    return if status == "running"
    self.end_time = start_time + duration.seconds
    self.duration = format_to_seconds(duration_time)
    save
  end

  def set_duration
    if duration.positive?
      self.duration_time = seconds_to_format(duration)
    elsif status == "running"
      self.duration_time = seconds_to_format((start_time - DateTime.current).ceil)
    end
  end
end
