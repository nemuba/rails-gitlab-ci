module ClocksHelper
  def seconds_to_format(time)
    ClockService.seconds_to_format(time)
  end

  def format_to_seconds(time)
    ClockService.format_to_seconds(time)
  end
end
