class CreateClocks < ActiveRecord::Migration[7.1]
  def change
    create_enum :clock_status, %w[running paused stopped], comment: 'Status of the clock'

    create_table :clocks do |t|
      t.string :description, null: false, comment: 'Description of the clock'
      t.datetime :start_time, null: false, default: -> { 'CURRENT_TIMESTAMP' }, comment: 'Start time of the clock'
      t.datetime :end_time, comment: 'End time of the clock'
      t.datetime :paused_at, comment: 'Time when the clock was paused'
      t.enum :status, default: 'stopped', enum_type: 'clock_status', comment: 'Status of the clock'
      t.integer :duration, default: 0, comment: 'Duration of the clock in seconds'

      t.references :user, null: false, foreign_key: true, comment: 'User that created the clock'
      t.timestamps
    end

    add_index :clocks, :status
  end
end
