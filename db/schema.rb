# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_04_26_000948) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  # Custom types defined in this database.
  # Note that some types may not work with other database engines. Be careful if changing database.
  create_enum "clock_status", ["running", "paused", "stopped"]

  create_table "clocks", force: :cascade do |t|
    t.string "description", null: false, comment: "Description of the clock"
    t.datetime "start_time", null: false, comment: "Start time of the clock"
    t.datetime "end_time", comment: "End time of the clock"
    t.datetime "paused_at", comment: "Time when the clock was paused"
    t.enum "status", default: "stopped", comment: "Status of the clock", enum_type: "clock_status"
    t.integer "duration", default: 0, comment: "Duration of the clock in seconds"
    t.bigint "user_id", null: false, comment: "User that created the clock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["status"], name: "index_clocks_on_status"
    t.index ["user_id"], name: "index_clocks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "clocks", "users"
end
