# frozen_string_literal: true

require "test_helper"

class ClockServiceTest < ActiveSupport::TestCase
  test "ClockService should format time to seconds" do
    assert_equal 750, ClockService.format_to_seconds("12m 30s")
    assert_equal 720, ClockService.format_to_seconds("12m")
    assert_equal 4350, ClockService.format_to_seconds("1h 12m 30s")
    assert_equal 3600, ClockService.format_to_seconds("1h")
  end

  test "ClockService should raise an error if time format is invalid" do
    assert_raises ArgumentError, "Invalid time format. Please use the format '12h 30m 30s'" do
      ClockService.format_to_seconds("12m 30")
    end

    assert_raises ArgumentError, "Invalid time format. Please use the format '12h 30m 30s'" do
      ClockService.format_to_seconds("12m 30s 30")
    end
  end

  test "ClockService should format seconds to time" do
    assert_equal "12m 30s", ClockService.seconds_to_format(750)
    assert_equal "1h 12m 30s", ClockService.seconds_to_format(4350)
    assert_equal "1h", ClockService.seconds_to_format(3600)
  end

  test "ClockService should create a clock" do
    duration = ClockService.format_to_seconds("12m 30s")
    params = { description: "Clock 1", duration: duration, status: "stopped" }
    current_user = User.create(email: Faker::Internet.email, password: "password", password_confirmation: "password")

    ClockService.new(params, current_user).create_clock

    assert_equal 1, current_user.clocks.count
    assert_equal 750, current_user.clocks.first.duration
  end
end
