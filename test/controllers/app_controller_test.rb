require "test_helper"

class AppControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    @user = User.create(email: Faker::Internet.email, password: "password", password_confirmation: "password")
    sign_in(@user)
    get root_path
    assert_response :success
  end

  test "should redirect to login" do
    get root_path
    assert_redirected_to new_user_session_path
  end
end
