require "test_helper"

class ClockTest < ActiveSupport::TestCase
  test "Clock should have a description" do
    clock = Clock.new
    assert_not clock.save
  end

  test "Clock should have a user" do
    clock = Clock.new(description: "Clock 1")
    assert_not clock.save
  end

  test "Clock status should be default" do
    clock = Clock.new(description: "Clock 1")
    assert_equal "stopped", clock.status
  end

  test "Clock start_time should't be nil" do
    clock = Clock.new(description: "Clock 1")
    assert_not clock.start_time.nil?
  end

  test "Clock paused_at should be nil" do
    clock = Clock.new(description: "Clock 1")
    assert clock.paused_at.nil?
  end

  test "Clock duration should be 0" do
    clock = Clock.new(description: "Clock 1")
    assert_equal 0, clock.duration
  end
end
