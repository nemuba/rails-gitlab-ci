require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "user attributes must not be empty" do
    user = User.new
    assert user.invalid?
    assert user.errors[:email].any?
    assert user.errors[:password].any?
  end

  test "user email must be unique" do
    user = User.new(email: users(:one).email)
    assert user.invalid?
    assert_equal [ "has already been taken" ], user.errors[:email]
  end

  test "user email must be valid" do
    user = User.new(email: "invalid")
    assert user.invalid?
    assert_equal [ "is invalid" ], user.errors[:email]
  end

  test "user password must be at least 6 characters" do
    user = User.new(password: "short")
    assert user.invalid?
    assert_equal [ "is too short (minimum is 6 characters)" ], user.errors[:password]
  end

  test "user password must be at most 20 characters" do
    user = User.new(password: "long" * 10)
    assert user.invalid?
    assert_equal [ "is too long (maximum is 20 characters)" ], user.errors[:password]
  end

  test "user password must be present" do
    user = User.new(password: nil)
    assert user.invalid?
    assert_includes user.errors[:password], "can't be blank"
  end

  test "user valid with email and password" do
    user = User.new(email: Faker::Internet.email, password: "password", password_confirmation: "password")
    assert user.valid?
  end

  test "user has many clocks" do
    user = users(:one)
    user.clocks.create!(description: "Clock 1")
    assert user.clocks.any?
  end
end
