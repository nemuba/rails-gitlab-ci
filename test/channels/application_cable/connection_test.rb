require "test_helper"

module ApplicationCable
  class ConnectionTest < ActionCable::Connection::TestCase
    test "connects with a user" do
      # Crie um usuário de teste
      user = User.create!(email: "test@example.com", password: "password", password_confirmation: "password")

      # Simule um usuário autenticado
      cookies.signed["user.id"] = user.id
      cookies.signed["user.expires_at"] = 1.hour.from_now

      # Conecta
      connect "/cable"

      # Verifica se a conexão foi bem-sucedida
      assert_equal connection.current_user.id, user.id
    end
  end
end
